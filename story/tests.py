from django.test import TestCase, Client
from django.urls import resolve
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from django.contrib.staticfiles.testing import StaticLiveServerTestCase

from story.views import index
import time

class StoryTest(TestCase):

    def test_index_page_response_success(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_index_page_use_index_template(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'index.html')

    def test_index_page_use_index_function(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

class FunctionalTest(StaticLiveServerTestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')

        self.browser = webdriver.Chrome(chrome_options=chrome_options)
        self.browser.get('http://localhost:8000')

    def tearDown(self):
        self.browser.close()

    def test_index_use_the_index_template(self):
        self.assertEqual('Story 8', self.browser.title)

    def test_move_accordion_down(self):
        acc_head_btn = self.browser.find_elements_by_class_name("btn-down")
        acc_head_btn[0].click()

        acc_head = self.browser.find_elements_by_class_name("acc-head")
        self.assertEqual('Who am I', acc_head[1].text)

    def test_move_accordion_up(self):
        acc_head_btn = self.browser.find_elements_by_class_name("btn-up")
        acc_head_btn[1].click()

        acc_head = self.browser.find_elements_by_class_name("acc-head")
        self.assertEqual('Current Activities', acc_head[0].text)

    def test_change_theme_button(self):
        body_color = self.browser.find_element_by_tag_name('body').value_of_css_property('background-color')
        acc_head_color = self.browser.find_element_by_class_name('acc-head').value_of_css_property('background-color')
        acc_body_color = self.browser.find_element_by_class_name('acc-body').value_of_css_property('background-color')
        self.assertEqual(body_color, "rgba(255, 255, 255, 1)")
        self.assertEqual(acc_head_color, "rgba(81, 43, 88, 1)")
        self.assertEqual(acc_body_color, "rgba(164, 209, 208, 1)")

        change_theme = self.browser.find_element_by_id('changeColor')
        change_theme.click()

        new_body_color = self.browser.find_element_by_tag_name('body').value_of_css_property('background-color')
        new_acc_head_color = self.browser.find_element_by_class_name('acc-head').value_of_css_property('background-color')
        new_acc_body_color = self.browser.find_element_by_class_name('acc-body').value_of_css_property('background-color')
        self.assertEqual(new_body_color, "rgba(34, 34, 34, 1)")
        self.assertEqual(new_acc_head_color, "rgba(47, 79, 79, 1)")
        self.assertEqual(new_acc_body_color, "rgba(128, 128, 128, 1)")
