$(function() {
  $('#accordion').accordion({
    collapsible: true
  });
});
$('.btn-down').on('click', function(){
  var currentHead = $(this).parent();
  var currentBody = currentHead.next('.acc-body');
  var nextHead = currentBody.next('.acc-head');
  var nextBody = nextHead.next('.acc-body');
  currentHead.insertAfter(nextBody);
  currentBody.insertAfter(currentHead);
});
$('.btn-up').on('click', function(){
  var currentHead = $(this).parent();
  var currentBody = currentHead.next('.acc-body');
  var prevBody = currentHead.prev('.acc-body');
  var prevHead = prevBody.prev('.acc-head');
  currentHead.insertBefore(prevHead);
  currentBody.insertAfter(currentHead);
});
