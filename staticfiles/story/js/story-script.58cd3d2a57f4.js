$(function() {
  $('#accordion').accordion();
});

$('.btn-down').on('click', function(){
  var currentHead = $(this).parent();
  var currentBody = currentHead.next('.acc-body');
  var nextHead = currentBody.next('.acc-head');
  var nextBody = nextHead.next('.acc-body');
  currentHead.insertAfter(nextBody);
  currentBody.insertAfter(currentHead);
});

$('.btn-up').on('click', function(){
  var currentHead = $(this).parent();
  var currentBody = currentHead.next('.acc-body');
  var prevBody = currentHead.prev('.acc-body');
  var prevHead = prevBody.prev('.acc-head');
  currentHead.insertBefore(prevHead);
  currentBody.insertAfter(currentHead);
});

function changeColor() {
  var cardHeader = document.getElementsByClassName("card-header");
  var cardBody = document.getElementsByClassName("card-body");
  var i;
  for (i = 0; i < cardHeader.length; i++) {
  cardHeader[i].style.color = "red";
  }
  for (i = 0; i < cardBody.length; i++) {
  cardBody[i].style.color = "blue";
  }
}
