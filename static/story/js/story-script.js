$(function() {
  $('#accordion').accordion();
});

$('.btn-down').on('click', function(){
  var currentHead = $(this).parent();
  var currentBody = currentHead.next('.acc-body');
  var nextHead = currentBody.next('.acc-head');
  var nextBody = nextHead.next('.acc-body');
  currentHead.insertAfter(nextBody);
  currentBody.insertAfter(currentHead);
});

$('.btn-up').on('click', function(){
  var currentHead = $(this).parent();
  var currentBody = currentHead.next('.acc-body');
  var prevBody = currentHead.prev('.acc-body');
  var prevHead = prevBody.prev('.acc-head');
  currentHead.insertBefore(prevHead);
  currentBody.insertAfter(currentHead);
});

$('#changeColor').on('click', function(){
  var currentColor = $("body").css("background-color");
  var accBody = document.getElementsByClassName("acc-body");
  if (currentColor == "rgb(255, 255, 255)") {
    $("body").css("background-color", "rgb(34, 34, 34)");
    $("button").css("background-color", "rgb(128, 128, 128)");
    $("button").css("color", "rgb(211, 211, 211)");
    $("p").css("color", "rgb(211, 211, 211)");
    $("li").css("color", "rgb(211, 211, 211)");
    $("h2").css("color", "rgb(211, 211, 211)");
    $("h3").css("color", "rgb(211, 211, 211)");
    $("h4").css("color", "rgb(211, 211, 211)");
    $(".acc-head").css("background-color", "rgb(47, 79, 79)");
    $(".acc-body").css("background-color", "rgb(128, 128, 128)");
    $("li").css("background-color", "rgb(128, 128, 128)");
  } else {
    $("body").css("background-color", "rgb(255, 255, 255)");
    $("button").css("background-color", "rgb(19, 132, 150)");
    $("button").css("color", "rgb(240, 255, 255)");
    $("p").css("color", "rgb(0, 0, 0)");
    $("li").css("color", "rgb(0, 0, 0)");
    $("h2").css("color", "rgb(0, 0, 0)");
    $("h3").css("color", "rgb(236, 236, 236)");
    $("h4").css("color", "rgb(0, 0, 0)");
    $(".acc-head").css("background-color", "rgb(81, 43, 88)");
    $(".acc-body").css("background-color", "rgb(164, 209, 208)");
    $("li").css("background-color", "rgb(164, 209, 208)");
  }
});
